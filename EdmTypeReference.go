package OData

type IEdmTypeReference interface {
	GetIsNullable() bool
	GetDefinition() IEdmType
	ToString() string
}

type EdmTypeReference struct {
	Definition IEdmType
	IsNullable bool
}

func NewEdmTypeReference(definition IEdmType, isNullable bool) *EdmTypeReference {
	return &EdmTypeReference{
		Definition: definition,
		IsNullable: isNullable,
	}
}

func (e *EdmTypeReference) GetIsNullable() bool {
	return e.IsNullable
}

func (e *EdmTypeReference) GetDefinition() IEdmType {
	return e.Definition
}

func (e *EdmTypeReference) ToString() string {
	return e.Definition.ToString()
}
