package OData

import (
	"log"
)

type IEdmStructuredType interface {
	FindProperty(string)
	AddStructuralProperty(name string, typeKind EdmPrimitiveTypeKind) *EdmStructuralProperty
}

type EdmStructuredType struct {
	EdmType
	BaseType             interface{}
	IsAbstract           bool
	IsOpen               bool
	DeclaredProperties   []IEdmProperty
	propertiesDictionary map[string]IEdmProperty
}

func NewEdmStructuredType(isAbstract, isOpen bool, baseType EdmStructuredType) *EdmStructuredType {
	return &EdmStructuredType{
		IsAbstract: isAbstract,
		IsOpen:     isOpen,
		BaseType:   baseType,
	}
}

func (s *EdmStructuredType) FindProperty(prop string) {
	log.Println("EdmStructuredType called.....")
}

func (s *EdmStructuredType) AddProperty(property IEdmProperty) {
	s.DeclaredProperties = append(s.DeclaredProperties, property)
}

func (s *EdmStructuredType) AddStructuralProperty(name string, typeKind EdmPrimitiveTypeKind) *EdmStructuralProperty {
	core := NewEdmCoreModel()
	property := NewEdmStructuralProperty(
		*s,
		name,
		NewEdmTypeReference(core.GetPrimitive(typeKind, false).Definition, false))
	s.AddProperty(property)
	return property
}
