package OData

type IEdmEntitySet interface {
	IEdmEntitySetBase
	IEdmEntityContainerElement
	GetIncludeInServiceDocument() bool
}

type EdmEntitySet struct {
	EdmEntitySetBase
	Container IEdmEntityContainer
	//Path IEdmPathExpression
	IncludeInServiceDocument bool
}

func (e *EdmEntitySet) GetContainerElementKind() EdmContainerElementKind {
	return ContainerEntitySet
}

func (e *EdmEntitySet) GetContainer() IEdmEntityContainer {
	return e.Container
}

func (e *EdmEntitySet) GetIncludeInServiceDocument() bool {
	return e.IncludeInServiceDocument
}

func NewEdmEntitySet(container IEdmEntityContainer, name string, elementType IEdmEntityType) *EdmEntitySet {
	return &EdmEntitySet{
		EdmEntitySetBase: *NewEdmEntitySetBase(name, elementType),
		Container:        container,
	}
}
