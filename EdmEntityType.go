package OData

// EdmEntityTypeOption type
type EdmEntityTypeOption func(a *EdmEntityType)

// WithName func
func WithName(name string) EdmEntityTypeOption {
	return func(e *EdmEntityType) {
		e.Name = name
	}
}

// WithNamespace func
func WithNamespace(namespace string) EdmEntityTypeOption {
	return func(e *EdmEntityType) {
		e.Namespace = namespace
	}
}

// WithBaseType func
func WithBaseType(baseType IEdmStructuredType) EdmEntityTypeOption {
	return func(e *EdmEntityType) {
		e.BaseType = baseType
	}
}

// WithIsOpen func
func WithIsOpen(isOpen bool) EdmEntityTypeOption {
	return func(e *EdmEntityType) {
		e.IsOpen = isOpen
	}
}

// WithIsAbstract func
func WithIsAbstract(isAbstract bool) EdmEntityTypeOption {
	return func(e *EdmEntityType) {
		e.IsAbstract = isAbstract
	}
}

// WithHasStream func
func WithHasStream(hasStream bool) EdmEntityTypeOption {
	return func(e *EdmEntityType) {
		e.HasStream = hasStream
	}
}

type IEdmEntityType interface {
	GetDeclaredKey() []IEdmStructuralProperty
	GetHasStream() bool
	IEdmFullNamedElement
	IEdmType
}

type EdmEntityType struct {
	EdmStructuredType
	Name        string
	Namespace   string
	FullName    string
	HasStream   bool
	DeclaredKey []IEdmStructuralProperty
}

func NewEdmEntityType(options ...EdmEntityTypeOption) *EdmEntityType {
	return newEdmEntityType(options...)
}

func newEdmEntityType(option ...EdmEntityTypeOption) *EdmEntityType {
	e := &EdmEntityType{}
	for _, typeOption := range option {
		typeOption(e)
	}

	if len(e.Namespace) != 0 && len(e.Name) != 0 {
		e.FullName = e.Namespace + "." + e.Name
	}

	e.TypeKind = Entity

	// TODO: find the proper way to send
	//  pointer of EdmEntityType struct
	return e
}

func (e *EdmEntityType) GetDeclaredKey() []IEdmStructuralProperty {
	return e.DeclaredKey
}

func (e *EdmEntityType) GetHasStream() bool {
	return e.HasStream || e.BaseType != nil && e.BaseType.(*EdmEntityType).HasStream
}

func (e *EdmEntityType) GetSchemaElementKind() EdmSchemaElementKind {
	return SchemaTypeDefinition
}

func (e *EdmEntityType) GetNamespace() string {
	return e.Namespace
}

func (e *EdmEntityType) GetName() string {
	return e.Name
}

func (e *EdmEntityType) GetFullName() string {
	return e.FullName
}

func (e *EdmEntityType) GetTypeKind() EdmTypeKind {
	return Entity
}

func (e *EdmEntityType) AddKey(keyProperties IEdmStructuralProperty) {
	e.DeclaredKey = append(e.DeclaredKey, keyProperties)
}
