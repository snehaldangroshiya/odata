package OData

type IEdmEntityContainerElement interface {
	IEdmNamedElement
	GetContainerElementKind() EdmContainerElementKind
	//IEdmVocabularyAnnotatable
	GetContainer() IEdmEntityContainer
}

type EdmContainerElementKind int

const (
	ContainerEntitySet EdmContainerElementKind = iota + 1
	ContainerActionImport
	ContainerFunctionImport
	ContainerSingleton
)
