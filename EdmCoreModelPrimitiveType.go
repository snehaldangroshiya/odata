package OData

type EdmCoreModelPrimitiveType struct {
	Name          string
	PrimitiveKind EdmPrimitiveTypeKind
	FullName      string
}

func NewEdmCoreModelPrimitiveType(primitiveKind EdmPrimitiveTypeKind) EdmCoreModelPrimitiveType {
	cPrimitiveType := EdmCoreModelPrimitiveType{
		Name:          primitiveKind.ToString(),
		PrimitiveKind: primitiveKind,
	}

	cPrimitiveType.FullName = cPrimitiveType.Namespace() + "." + cPrimitiveType.Name
	return cPrimitiveType
}

func (e EdmCoreModelPrimitiveType) GetPrimitiveKind() EdmPrimitiveTypeKind {
	return e.PrimitiveKind
}

func (e EdmCoreModelPrimitiveType) GetTypeKind() EdmTypeKind {
	return Primitive
}

func (e *EdmCoreModelPrimitiveType) Namespace() string {
	return EdmNamespace
}

func (e EdmCoreModelPrimitiveType) ToString() string {
	return e.PrimitiveKind.ToString()
}
