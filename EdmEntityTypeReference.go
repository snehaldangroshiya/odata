package OData

type IEdmEntityTypeReference interface {
	IEdmTypeReference
}

type EdmEntityTypeReference struct {
	EdmTypeReference
}

func NewEdmEntityTypeReference(entityType *EdmEntityType, isNullable bool) *EdmEntityTypeReference {
	return &EdmEntityTypeReference{
		EdmTypeReference{
			Definition: entityType,
			IsNullable: isNullable,
		},
	}
}
