package OData

type IEdmPrimitiveTypeReference interface {
}

type EdmPrimitiveTypeReference struct {
	EdmTypeReference
}

func NewEdmPrimitiveTypeReference(definition IEdmPrimitiveType, isNullable bool) EdmPrimitiveTypeReference {
	return EdmPrimitiveTypeReference{*NewEdmTypeReference(definition, isNullable)}
}

//
//func (e EdmPrimitiveTypeReference) IsNullable() bool {
//	return e.EdmTypeReference.IsNullable
//}
//
//func (e EdmPrimitiveTypeReference) Definition() IEdmType {
//	return e.EdmTypeReference.Definition
//}
//
//func (e EdmPrimitiveTypeReference) ToString() string {
//	return e.EdmTypeReference.ToString()
//}
