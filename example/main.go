package main

import (
	"github.com/snehal1112/OData"
)

func main() {

	container := OData.NewEdmEntityContainer("WebApiDocNS", "Container")
	customer := OData.NewEdmEntityType(OData.WithNamespace("WebApiDocNS"), OData.WithName("Customer"), OData.WithIsOpen(true))
	customer.HasStream = false
	customer.AddKey(customer.AddStructuralProperty("CustomerID", OData.Int16))
	customer.AddKey(customer.AddStructuralProperty("KEY", OData.String))
	customer.AddStructuralProperty("Location", OData.String)
	container.AddEntitySet("Customer", customer)

	localCustomer := OData.NewEdmEntityType(OData.WithNamespace("WebApiDocNS"), OData.WithName("LocalCustomer"), OData.WithBaseType(customer))
	localCustomer.AddStructuralProperty("Name", OData.String)
	container.AddEntitySet("LocalCustomer", localCustomer)

	vipCustomer := OData.NewEdmEntityType(OData.WithNamespace("WebApiDocNS"), OData.WithIsOpen(true), OData.WithName("VipCustomer"), OData.WithBaseType(localCustomer))
	vipCustomer.AddStructuralProperty("Address", OData.String)
	container.AddEntitySet("VipCustomer", vipCustomer)

	OData.Dump(vipCustomer, "vipcustomer")

	core := OData.NewEdmCoreModel()
	OData.Dump(core.GetCoreModelPrimitiveType(OData.Boolean), "GetCoreModelPrimitiveType")

	OData.Dump(vipCustomer.GetDeclaredKey(), "vipCustomer.GetDeclaredKey()[0]")

	props := OData.NewEdmStructuralProperty(
		*OData.NewEdmStructuredType(false, false, OData.EdmStructuredType{}),
		"sd",
		OData.NewEdmTypeReference(
			core.GetPrimitive(OData.Int16, false).Definition,
			false,
		),
	)
	OData.Dump(props, "props")

	OData.Dump(container, "entitySetDictionary:-")
	OData.Dump(container.FindEntitySet("Customer"), "VipCustomer")
}
