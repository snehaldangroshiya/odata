package OData

type IEdmFullNamedElement interface {
	GetFullName() string
	IEdmNamedElement
}

type IEdmNamedElement interface {
	GetName() string
}

type EdmNamedElement struct {
	Name string
}

func NewEdmNamedElement(name string) EdmNamedElement {
	return EdmNamedElement{Name: name}
}

func (e *EdmNamedElement) GetName() string {
	return e.Name
}
