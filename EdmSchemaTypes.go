package OData

type IEdmSchemaType interface {
	IEdmSchemaElement
	IEdmType
}
