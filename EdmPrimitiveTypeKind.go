package OData

type IEdmPrimitiveType interface {
	GetPrimitiveKind() EdmPrimitiveTypeKind
	IEdmType
}

type EdmPrimitiveTypeKind int

const (
	Binary EdmPrimitiveTypeKind = iota + 0
	Boolean
	Byte
	DateTimeOffset
	Decimal
	Double
	Guid
	Int16
	Int32
	Int64
	SByte
	Single
	String
	Stream
	Duration
	Geography
	GeographyPoint
	GeographyLineString
	GeographyPolygon
	GeographyCollection
	GeographyMultiPolygon
	GeographyMultiLineString
	GeographyMultiPoint
	Geometry
	GeometryPoint
	GeometryLineString
	GeometryPolygon
	GeometryCollection
	GeometryMultiPolygon
	GeometryMultiLineString
	GeometryMultiPoint
	Date
	TimeOfDay
	PrimitiveType
)

func (e EdmPrimitiveTypeKind) ToString() (value string) {
	return [...]string{
		"Binary",
		"Boolean",
		"Byte",
		"DateTimeOffset",
		"Decimal",
		"Double",
		"Guid",
		"Int16",
		"Int32",
		"Int64",
		"SByte",
		"Single",
		"String",
		"Stream",
		"Duration",
		"GeographyPoint",
		"Geography",
		"GeographyLineString",
		"GeographyPolygon",
		"GeographyCollection",
		"GeographyMultiPolygon",
		"GeographyMultiLineString",
		"GeographyMultiPoint",
		"Geometry",
		"GeometryPoint",
		"GeometryLineString",
		"GeometryPolygon",
		"GeometryCollection",
		"GeometryMultiPolygon",
		"GeometryMultiLineString",
		"GeometryMultiPoint",
		"Date",
		"TimeOfDay",
		"PrimitiveType",
	}[e]
}

func (e EdmPrimitiveTypeKind) GetPrimitiveKind() EdmPrimitiveTypeKind {
	return 0
}
