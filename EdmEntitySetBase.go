package OData

type IEdmEntitySetBase interface {
	IEdmNavigationSource
}

type EdmEntitySetBase struct {
	EdmNavigationSource
	CollectionType IEdmCollectionType
}

func NewEdmEntitySetBase(name string, elementType IEdmEntityType) *EdmEntitySetBase {
	return &EdmEntitySetBase{
		EdmNavigationSource: *NewEdmNavigationSource(name),
		CollectionType:      NewEdmCollectionType(NewEdmTypeReference(elementType, false)),
	}
}

func (e *EdmEntitySetBase) GetType() IEdmType {
	return e.CollectionType
}
