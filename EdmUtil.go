package OData

import (
	"encoding/json"
	"log"
)

func Dump(value interface{}, label string) string {
	jsonValue, err := json.MarshalIndent(value, "", "  ")
	if err != nil {
		panic(err)
	}

	log.Println(label+" :-", string(jsonValue))
	return string(jsonValue)
}
