package OData

import "log"

type IEdmEntityContainer interface {
	IEdmSchemaElement
	IEdmNamedElement
	GetElements() []IEdmEntityContainerElement
	FindEntitySet(setName string) IEdmEntitySet
	//FindSingleton(singletonName string) IEdmSingleton
	//FindOperationImports(operationName string) IEdmOperationImport
}

type EdmEntityContainer struct {
	EdmElement
	Name      string
	Namespace string
	FullName  string

	containerElements   []IEdmEntityContainerElement
	entitySetDictionary map[string]IEdmEntitySet
}

func NewEdmEntityContainer(namespaceName, name string) *EdmEntityContainer {
	return &EdmEntityContainer{
		Namespace:           namespaceName,
		Name:                name,
		FullName:            namespaceName + "." + name,
		entitySetDictionary: make(map[string]IEdmEntitySet),
	}
}

func (e *EdmEntityContainer) GetElements() []IEdmEntityContainerElement {
	return e.containerElements
}

func (e *EdmEntityContainer) GetName() string {
	return e.Name
}

func (e *EdmEntityContainer) FindEntitySet(setName string) IEdmEntitySet {
	return e.entitySetDictionary[setName]
}

func (e *EdmEntityContainer) GetSchemaElementKind() EdmSchemaElementKind {
	return EntityContainer
}

func (e *EdmEntityContainer) GetNamespace() string {
	return e.Namespace
}

func (e *EdmEntityContainer) GetFullName() string {
	return e.FullName
}

func (e *EdmEntityContainer) AddElement(element IEdmEntityContainerElement) {
	log.Println(element.GetContainerElementKind() == ContainerEntitySet)
	switch element.GetContainerElementKind() {
	case ContainerEntitySet:
		e.entitySetDictionary[element.GetName()] = element.(IEdmEntitySet)
		break
	case ContainerSingleton:
		log.Println("Register entity set ContainerSingleton")
		break
	case ContainerActionImport:
	case ContainerFunctionImport:
		log.Println("Register entity set ContainerFunctionImport")
		break
	}
}

func (e *EdmEntityContainer) AddEntitySet(name string, element IEdmEntityType) *EdmEntitySet {
	entitySet := NewEdmEntitySet(e, name, element)
	e.AddElement(entitySet)
	return entitySet
}
