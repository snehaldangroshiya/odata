package OData

type EdmModelBase struct {
	EdmElement
}

func NewEdmModelBase() *EdmModelBase {
	return &EdmModelBase{}
}
