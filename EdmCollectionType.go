package OData

type IEdmCollectionType interface {
	IEdmType
	GetElementType() IEdmTypeReference
}

type EdmCollectionType struct {
	ElementType IEdmTypeReference
	EdmType
}

func NewEdmCollectionType(elementType IEdmTypeReference) *EdmCollectionType {
	return &EdmCollectionType{ElementType: elementType}
}

func (e *EdmCollectionType) GetElementType() IEdmTypeReference {
	return e.ElementType
}

func (e *EdmCollectionType) GetTypeKind() EdmTypeKind {
	return Collection
}
