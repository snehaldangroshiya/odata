package OData

var primitiveType EdmCoreModelPrimitiveType = NewEdmCoreModelPrimitiveType(PrimitiveType)

var Core *EdmCoreModel

type EdmCoreModel struct {
	PrimitiveTypeKinds   map[string]EdmPrimitiveTypeKind
	PrimitiveTypesByKind map[EdmPrimitiveTypeKind]EdmCoreModelPrimitiveType
}

func NewEdmCoreModel() *EdmCoreModel {
	if Core != nil {
		return Core
	}

	coreModel := &EdmCoreModel{
		PrimitiveTypeKinds:   make(map[string]EdmPrimitiveTypeKind),
		PrimitiveTypesByKind: make(map[EdmPrimitiveTypeKind]EdmCoreModelPrimitiveType),
	}

	primitiveTypes := []EdmCoreModelPrimitiveType{
		NewEdmCoreModelPrimitiveType(Int64),
		NewEdmCoreModelPrimitiveType(Double),
		NewEdmCoreModelPrimitiveType(Single),
		NewEdmCoreModelPrimitiveType(Int32),
		NewEdmCoreModelPrimitiveType(Int16),
		NewEdmCoreModelPrimitiveType(SByte),
		NewEdmCoreModelPrimitiveType(Byte),
		NewEdmCoreModelPrimitiveType(Boolean),
		NewEdmCoreModelPrimitiveType(Guid),
		NewEdmCoreModelPrimitiveType(Duration),
		NewEdmCoreModelPrimitiveType(TimeOfDay),
		NewEdmCoreModelPrimitiveType(DateTimeOffset),
		NewEdmCoreModelPrimitiveType(Date),
		NewEdmCoreModelPrimitiveType(Decimal),
		NewEdmCoreModelPrimitiveType(Binary),
		NewEdmCoreModelPrimitiveType(String),
		NewEdmCoreModelPrimitiveType(Stream),
		NewEdmCoreModelPrimitiveType(Geography),
		NewEdmCoreModelPrimitiveType(GeographyPoint),
		NewEdmCoreModelPrimitiveType(GeographyLineString),
		NewEdmCoreModelPrimitiveType(GeographyPolygon),
		NewEdmCoreModelPrimitiveType(GeographyCollection),
		NewEdmCoreModelPrimitiveType(GeographyMultiPolygon),
		NewEdmCoreModelPrimitiveType(GeographyMultiLineString),
		NewEdmCoreModelPrimitiveType(GeographyMultiPoint),
		NewEdmCoreModelPrimitiveType(Geometry),
		NewEdmCoreModelPrimitiveType(GeometryPoint),
		NewEdmCoreModelPrimitiveType(GeometryLineString),
		NewEdmCoreModelPrimitiveType(GeometryPolygon),
		NewEdmCoreModelPrimitiveType(GeometryCollection),
		NewEdmCoreModelPrimitiveType(GeometryMultiPolygon),
		NewEdmCoreModelPrimitiveType(GeometryMultiLineString),
		NewEdmCoreModelPrimitiveType(GeometryMultiPoint),
		primitiveType,
	}

	for _, primitive := range primitiveTypes {
		coreModel.PrimitiveTypeKinds[primitive.Name] = primitive.PrimitiveKind
		coreModel.PrimitiveTypeKinds[primitive.Namespace()+"."+primitive.Name] = primitive.PrimitiveKind
		coreModel.PrimitiveTypesByKind[primitive.PrimitiveKind] = primitive
	}

	Core = coreModel
	return coreModel
}

func (e EdmCoreModel) GetInt16(isNullable bool) EdmPrimitiveTypeReference {
	return NewEdmPrimitiveTypeReference(e.GetCoreModelPrimitiveType(Int16), isNullable)
}

func (e EdmCoreModel) GetCoreModelPrimitiveType(kind EdmPrimitiveTypeKind) EdmCoreModelPrimitiveType {
	return e.PrimitiveTypesByKind[kind]
}

func (e EdmCoreModel) GetPrimitive(kind EdmPrimitiveTypeKind, isNullable bool) EdmPrimitiveTypeReference {
	return NewEdmPrimitiveTypeReference(e.GetCoreModelPrimitiveType(kind), isNullable)
}

func (e EdmCoreModel) GetPrimitiveType(kind EdmPrimitiveTypeKind) IEdmPrimitiveType {
	return e.GetCoreModelPrimitiveType(kind)
}
