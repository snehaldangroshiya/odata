package OData

type IEdmSchemaElement interface {
	GetSchemaElementKind() EdmSchemaElementKind
	GetNamespace() string
}

type EdmSchemaElementKind int

const (
	SchemaTypeDefinition EdmSchemaElementKind = iota + 1
	Term
	Action
	EntityContainer
	Function
)
