package OData

type EdmNavigationSourceKind int

const (
	EntitySet EdmNavigationSourceKind = iota + 1
	Singleton
	ContainedEntitySet
	UnknownEntitySet
)

type IEdmNavigationSource interface {
	//GetNavigationPropertyBindings() IEdmNavigationPropertyBinding
	GetType() IEdmType
	FindNavigationTarget() IEdmNavigationSource
	FindNavigationTargetWithPath() IEdmNavigationSource
	//FindNavigationPropertyBindings() IEdmNavigationPropertyBinding
}

type EdmNavigationSource struct {
	EdmNamedElement
}

func NewEdmNavigationSource(name string) *EdmNavigationSource {
	return &EdmNavigationSource{EdmNamedElement{name}}
}

func (e EdmNavigationSource) FindNavigationTarget() IEdmNavigationSource {
	panic("implement me")
}

func (e EdmNavigationSource) FindNavigationTargetWithPath() IEdmNavigationSource {
	panic("implement me")
}
