package OData

import "log"

type IEdmType interface {
	ToString() string
	GetTypeKind() EdmTypeKind
}

type EdmTypeKind int

const (
	None EdmTypeKind = iota
	Primitive
	Entity
	Complex
	Collection
	EntityReference
	Enum
	TypeDefinition
	Untyped
	Path
)

func (et EdmTypeKind) ToString() string {
	log.Println("ET:-", et)
	return [...]string{"NONE", "Primitive", "Entity", "Complex", "Collection", "EntityReference", "Enum", "TypeDefinition", "Untyped", "Path"}[et]
}

type EdmType struct {
	TypeKind EdmTypeKind
	EdmElement
}

func (e *EdmType) GetTypeKind() EdmTypeKind {
	return e.TypeKind
}

func (e *EdmType) ToString() string {
	log.Println(e.TypeKind.ToString())
	return e.TypeKind.ToString()
}
