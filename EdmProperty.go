package OData

type IEdmProperty interface {
	GetPropertyKind() IEdmType
	GetType() EdmTypeReference           //IEdmTypeReference
	GetDeclaringType() EdmStructuredType //IEdmStructuredType

}

type EdmPropertyKind int

const (
	Unknown EdmPropertyKind = iota
	Structural
	Navigation
)

type EdmProperty struct {
	declaringType EdmStructuredType
	EdmTypeReference
	EdmNamedElement
}

func NewEdmProperty(declaringType EdmStructuredType, name string, typeKind EdmTypeReference) *EdmProperty {
	return &EdmProperty{declaringType,
		typeKind,
		NewEdmNamedElement(name)}
}

func (e *EdmProperty) GetPropertyKind() IEdmType {
	return e.Definition
}

func (e *EdmProperty) GetType() EdmTypeReference {
	return e.EdmTypeReference
}

func (e *EdmProperty) GetDeclaringType() EdmStructuredType {
	return e.declaringType
}
