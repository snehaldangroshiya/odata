package OData

type IEdmStructuralProperty interface {
	DefaultValueString() string
	IEdmProperty
}

type EdmStructuralProperty struct {
	defaultValueString string
	EdmProperty
}

func NewEdmStructuralProperty(declaringType EdmStructuredType, name string, typeKind *EdmTypeReference) *EdmStructuralProperty {
	return &EdmStructuralProperty{
		"",
		*NewEdmProperty(declaringType, name, *typeKind),
	}
}

func (e *EdmStructuralProperty) DefaultValueString() string {
	return e.defaultValueString
}

func (e *EdmStructuralProperty) Type() IEdmTypeReference {
	panic("implement me")
}

func (e *EdmStructuralProperty) DeclaringType() IEdmStructuredType {
	panic("implement me")
}
